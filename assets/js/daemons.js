function delay(t) {
  return new Promise(function(resolve) {
    setTimeout(resolve, t)
  });
}

async function runDaemonPrices() {

 // DAEMONS
 getSevabitInfo();
 getLuKaInfo();
 getBoldInfo();


 await delay(5000);
 runDaemonPrices();
}


function getSevabitInfo() {
  var info = jQuery.get('http://seed1.sevabit.com:22049/getinfo');

  info.done(function(response) {

    response = JSON.parse(response);

    jQuery('#h_sevabit').text("H: " + response.height);
  });


  info.fail(function(response) {
    jQuery('#h_sevabit').text('No Info');
  });

}


function getLuKaInfo() {
  var info =  jQuery.get('https://seed219.cryptoluka.cl/getinfo');

  info.done(function(response) {

    response = JSON.parse(response);

    jQuery('#h_luka').text("H: " + response.height);
  });


  info.fail(function(response) {
    jQuery('#h_luka').text('No Info');
  });

}

function getBoldInfo() {
  var info =  jQuery.get('http://pool.boldprivate.network:53941/getinfo');

  info.done(function(response) {

    response = JSON.parse(response);

    jQuery('#h_bold').text("H: " + response.height);
  });


  info.fail(function(response) {
    jQuery('#h_bold').text('No Info');
  });

}