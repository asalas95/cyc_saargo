<!DOCTYPE html>
<!-- saved from url=(0022)http://cyc.saargo.com/ -->
<html lang="en-EN" class="js_active vc_desktop vc_transform vc_transform wf-poppins-n4-inactive wf-poppins-n6-inactive wf-poppins-n5-inactive wf-inactive">
<head>
    <?php include 'common/header.php' ?>

    <style type="text/css">
    viewer-zoom-button {
        display: none;
    }

    input:focus, textarea:focus, select:focus{
      border:1px solid #fafafa;
      -webkit-box-shadow:0 0 6px #007eff;
      -moz-box-shadow:0 0 5px #007eff;
      box-shadow:0 0 5px #007eff;
      outline: none;
    }
</style>

</head>

<body id="top" class="home page-template page-template-page-templates page-template-template-onepage page-template-page-templatestemplate-onepage-php page page-id-285 wpb-js-composer js-comp-ver-5.5.4 vc_responsive ico-dark icos-theme-ver-1.0 wordpress-version-4.9.8  no-touch loaded" data-spy="scroll" data-target="#mainnav" data-offset="80">
    <div id="wrapper">

        <!-- Header --> 
        <header class="site-header is-sticky">
            <!-- Navbar -->
            <div class="navbar navbar-expand-lg is-transparent" id="mainnav">
                <nav class="container">

                    <h1 class="navbar-brand">
                        <a href="http://cyc.saargo.com/">
                            <img class="logo" src="<?=base_url();?>assets/img/logo-cyc.png" alt="Create Your Crypto">
                            <img class="logo-2" src="<?=base_url();?>assets/img/logo-cyc.png" alt="Create Your Crypto">
                        </a>
                    </h1>

                    <button class="navbar-toggler" type="button">
                        <span class="navbar-toggler-icon">
                            <span class="ti ti-align-justify"></span>
                        </span>
                    </button>

                    <div class="navbar-collapse justify-content-end">
                        <ul id="menu-onepage-menu" class="navbar-nav menu-top">
                            <li id="menu-item-530" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-530">
                                <a href="#What" class="nav-link">What is CYC?</a>
                            </li>
                            <li id="menu-item-532" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-532">
                                <a href="#solution" class="nav-link">Solution</a>
                            </li>
                            <li id="menu-item-533" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-533">
                                <a href="#development" class="nav-link">Development</a>
                            </li>
                            <li id="menu-item-534" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-534">
                                <a href="#stories" class="nav-link">Stories</a>
                            </li>
                            <li id="menu-item-539" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-539">
                                <a href="<?=base_url();?>index.php/Contact" class="nav-link">CONTACT US</a>
                            </li>
                        </ul>                        
                    </div>
                </nav>
            </div>
            <!-- End Navbar -->
        </header>
        <!-- End Header -->

        <section class="vc_rows wpb_row vc_row-fluid mobile-center banner-particle banner-curb vc_row-o-equal-height vc_row-o-content-middle vc_row-flex banner banner-full d-flex align-items-center">
            <div id="particles-js" class="particles-container particles-js">
                <canvas class="particles-js-canvas-el" width="1903" height="920" style="width: 100%; height: 100%;"></canvas>
            </div>
            <div class="container">
                <div class="banner-content">
                    <div class="row">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-1 vc_col-md-10">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element">
                                        <div class="wpb_wrapper">
                                            <div class="header-image-lobelia animated fadeInUp" data-animate="fadeInUp" data-delay="0.65" style="visibility: visible; animation-delay: 0.65s;">
                                                <img src="<?=base_url();?>assets/img/header-cyc.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-2 vc_col-md-8">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1538371421426 text-center header-txt">
                                        <div class="wpb_wrapper">
                                            <!--    <h1 class="animated fadeInUp" data-animate="fadeInUp" data-delay="0.95" style="visibility: visible; animation-delay: 0.95s;">We Create Your Own Cryptocurrency</h1> -->
                                            <p class="lead color-primary animated fadeInUp" data-animate="fadeInUp" data-delay="1.05" style="visibility: visible; animation-delay: 1.05s;">We offer a complete service so you <br class="d-none d-md-block">can launch your project.</p>
                                            <ul class="btns animated fadeInUp" data-animate="fadeInUp" data-delay="1.15" style="visibility: visible; animation-delay: 1.15s;">
                                                <li>
                                                    <a class="btn btn-alt" href="#development">START YOUR PROJECT</a>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
        <section id="What" class="vc_rows wpb_row vc_row-fluid section-pad section-bg vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
            <div class="container">
                <div class="row">
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-2 vc_col-md-8 vc_col-sm-offset-0"><div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="section-head text-center">
                                <h2 class="section-title animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">
                                    What is CYC?
                                    <span>WHAT</span>
                                </h2>
                            </div>


                            <div class="wpb_text_column wpb_content_element  vc_custom_1538371828084 text-center">
                                <div class="wpb_wrapper">
                                    <p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">Custom development of blockchain projects by highly trained professionals with experience in coin launching.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-animate="fadeInUp" data-delay="0.3" class="res-m-btm wpb_column vc_column_container vc_col-sm-5 animated fadeInUp" style="visibility: visible; animation-delay: 0.3s;">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_single_image wpb_content_element vc_align_center">

                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                        <img width="667" height="552" src="<?=base_url();?>assets/img/graph-cyc-a.png" class="vc_single_image-img attachment-full" alt="" srcset="http://cyc.saargo.com/wp-content/uploads/2018/10/graph-cyc-a.png 667w, http://cyc.saargo.com/wp-content/uploads/2018/10/graph-cyc-a-300x248.png 300w" sizes="(max-width: 667px) 100vw, 667px">
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-animate="fadeInUp" data-delay="0.4" class="wpb_column vc_column_container vc_col-sm-6 vc_col-sm-offset-1 animated fadeInUp" style="visibility: visible; animation-delay: 0.4s;">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="features-item-s2 d-flex line">
                                <div class="features-icon-s2">
                                    <img src="<?=base_url();?>assets/img/icon-decentralized.png" alt="icon">
                                </div>
                                <div class="features-texts-s2">
                                    <h5 class="features-title-s2">Guarantee</h5>
                                    <p>Assistance and technical service throughout the whole process.</p>
                                </div>
                            </div>


                            <div class="features-item-s2 d-flex line">
                                <div class="features-icon-s2">
                                    <img src="<?=base_url();?>assets/img/icon-crowd.png" alt="icon">
                                </div>
                                <div class="features-texts-s2">
                                    <h5 class="features-title-s2">Team</h5>
                                    <p>Team of professionals in real time.</p>
                                </div>
                            </div>


                            <div class="features-item-s2 d-flex ">
                                <div class="features-icon-s2">
                                    <img src="<?=base_url();?>assets/img/icon-reward.png" alt="icon">
                                </div>
                                <div class="features-texts-s2">
                                    <h5 class="features-title-s2">Gem Projects</h5>
                                    <p>Projects with true added value.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
    </section>
    <section id="solution" class="vc_rows wpb_row vc_row-fluid section-pad no-pt section-bg vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-2 vc_col-md-8 vc_col-sm-offset-0"><div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="section-head text-center">
                            <h2 class="section-title animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">
                                CYC SOLUTION            
                                <span>SOLUTION</span>
                            </h2>
                        </div>


                        <div class="wpb_text_column wpb_content_element  vc_custom_1538372797375 text-center">
                            <div class="wpb_wrapper">
                           <!--     <p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid solution">
                            <div data-animate="fadeInUp" data-delay="0.3" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-relational-blockchain.png" alt="features">
                                            <h5>Relational Blockchain</h5>
                                            <p>With a highly capable and secure network, we guarantee the safety of the team and users' coins</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="0.4" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-fund.png" alt="features">
                                            <h5>Fraud Reduction</h5>
                                            <p>We use certified online payment platforms</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="0.5" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-wallet.png" alt="features">
                                            <h5>Next Generation Wallet</h5>
                                            <p>Modern and secure desktop and mobile wallets, designed to fit the client's tastes</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="0.6" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-server.png" alt="features">
                                            <h5>Recovery Nodes</h5>
                                            <p>We offer a complete backup of all the nodes of the client's network</p>
                                            <div>99,9% UPTIME</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="0.7" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-connect.png" alt="features">
                                            <h5>Full Transparency</h5>
                                            <p>By uploading our work to a Github repository, we guarantee full transparency of the team's work</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="0.8" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-cash.png" alt="features">
                                            <h5>Very Low Fees</h5>
                                            <p>The best market prices in consulting and developments</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="0.9" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-decentralize-network.png" alt="features">
                                            <h5>Decentralized Network</h5>
                                            <p>We offer a complete decentralized ecosystem, as pure as the Blockchain is</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div data-animate="fadeInUp" data-delay="1" class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3 animated">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="features-box text-center">
                                            <img src="<?=base_url();?>assets/img/icon-crypto-currency.png" alt="features">
                                            <h5>Crypto Payment</h5>
                                            <p>We accept #cryptos as a payment method, because the team believes in their future</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
</section>


<section id="development" class="vc_rows wpb_row vc_row-fluid section-pad no-pt section-bg vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-2 vc_col-md-8 vc_col-sm-offset-0"><div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="section-head text-center">
                        <h2 class="section-title animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">
                            Quote your development            
                            <span>DEVELOPMENT</span>
                        </h2>
                    </div>

                    <div class="wpb_text_column wpb_content_element  vc_custom_1538372797375 text-center">
                        <div class="wpb_wrapper">
                            <p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">Choose the best plan according to your requirements. You can prepay the development or contact us.</p>
                        </div>
                    </div>

                    <div id="formContext">

                    </div>

                    <br/>


                    <span style="display: inline;">
                        <h2 class="section-title animated fadeInUp" style="display: inline;" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;"> Your project quote: </h2>

                        <h2 style="float: right; display: inline;" class="section-title animated fadeInUp"  data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;"> Ƀ <span id="quote">0.00</span> </h2>
                    </span>
                    <div>
                        <br/>
                        <input id="checkOK" type="checkbox" /> I have read and I accept the 
                        <a id="agree" href="javascript:doNothing()">terms and conditions</a>.
                    </div>
                    <div id="coinpaymentButton" style="text-align: right;">
                        <br/>
                        <img onclick="setupPayment()" src="<?=base_url()?>assets\img\buynow-wide-yellow.png">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php include 'common/paybutton.php'; ?>

</div>
<div class="clearfix"></div>
</section>



<section id="stories" class="vc_rows wpb_row vc_row-fluid section-pad no-pt section-bg vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-2 vc_col-md-8 vc_col-sm-offset-0">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="section-head text-center">
                            <h2 class="section-title animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">
                                Success Stories            
                                <span>SUCCESS</span>
                            </h2>
                        </div>
                    </div>


                    <div class="wpb_text_column wpb_content_element text-center" style="margin-top: 50px; margin-bottom: 50px">
                        <div class="wpb_wrapper">
                            <p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">Our clients approve our services, always remaining  
                                <strong>100% professional</strong>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row technologies" style="text-align: center;">              
                
                <div class="col-one-eighth vue">
                    <a href="https://www.sevabit.com/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/sevabit.gif" alt="SevaBit logo">
                        <h6>SevaBit</h6>
                    </a>
                    <h6 id="h_sevabit">No Info</h6>
                </div>

                <div class="col-one-eighth nodejs">
                    <a href="https://www.cryptoluka.cl/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/luka.svg" alt="LuKa logo">
                        <h6>LuKa</h6>
                    </a>
                    <h6 id="h_luka">No Info</h6>
                </div>

                <div class="col-one-eighth nodejs" >
                    <a href="https://purkproject.com/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/purk.svg" alt="Purk logo">
                        <h6>Purk</h6>
                    </a>
                    <h6 id="h_purk">No Info</h6>
                </div>

                <div class="col-one-eighth reactjs">
                    <a href="https://www.boldprivate.network/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/bold.png" alt="BOLD logo">
                        <h6>Bold</h6>
                    </a>
                    <h6 id="h_bold">No Info</h6>
                </div>


                 <div class="col-one-eighth nodejs" >
                    <a href="https://constellacoin.cl/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/constella.png" alt="Constella logo">
                        <h6>Constella</h6>
                    </a>
                    <h6 id="h_constella">No Info</h6>
                </div>

                <div class="col-one-eighth nodejs" >
                    <a href="https://ird.cash/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/iridium.png" alt="Iridium logo">
                        <h6>Iridium</h6>
                    </a>
                    <h6 id="h_iridium">No Info</h6>
                </div>


                <div class="col-one-eighth nodejs" >
               <!--     <a href="https://www.emetbg.com/" target="_blank">
                        <img src="<?=base_url()?>assets/coins/luka.svg" alt="EMETBG logo">
                        <h6>EmetBG</h6>
                    </a>
                    <h6 id="h_emetbg">No Info</h6> -->
                </div>

               
                
              
                <div class="col-one-eighth nodejs">
                 <!--   <img src="<?=base_url()?>assets/coins/luka.svg" alt="LuKa logo">
                    <h6>LuKa</h6> -->
                </div>
            </div>
        </div>

    </div>  <!-- MAYBE NOT -->



</div>


<svg class="spritesheet">
  <symbol id="icon-user" viewBox="0 0 32 32">
    <title>user</title>
    <path d="M18 22.082v-1.649c2.203-1.241 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h28c0-4.030-5.216-7.364-12-7.918z"></path>
  </symbol>
  <symbol id="icon-envelop" viewBox="0 0 32 32">
    <title>envelop</title>
    <path d="M29 4h-26c-1.65 0-3 1.35-3 3v20c0 1.65 1.35 3 3 3h26c1.65 0 3-1.35 3-3v-20c0-1.65-1.35-3-3-3zM12.461 17.199l-8.461 6.59v-15.676l8.461 9.086zM5.512 8h20.976l-10.488 7.875-10.488-7.875zM12.79 17.553l3.21 3.447 3.21-3.447 6.58 8.447h-19.579l6.58-8.447zM19.539 17.199l8.461-9.086v15.676l-8.461-6.59z"></path>
  </symbol>
</svg>


<?php include 'common/footer.php'; ?>



<script type="text/javascript">

    var URL = "index.php/";

    jQuery(document).ready(function() {

    // RUN DAEMONS
    runDaemonPrices();

    // HIDE FIELDS
    jQuery('#coinpaymentButton').hide();


    // INIT FORM
    var request = jQuery.ajax({
        method: "POST",
        url: URL + 'home/initForm',
        dataType: "text"
    });

    request.done(function (response) {
        if(response != "ERROR") {
            jQuery('#formContext').html(response);
            jQuery("#algo").select2();
        }
    });


    // PDF OPTIONS
    jQuery('#agree').vibox({
        src: 'index.php/home/agreements', //   https://www.cryptoluka.cl/WhitePaper_LuKa_v1.2_ESP.pdf',
        paddingColor: 'black', // custom color of content background
        padding: 30, // custom padding (integer)
        margin: 100, // margin from top and bottom(if responsive, integer)
        responsive: true, // responsive
        width: 700, // max-width or fixed width( integer )
        height: 850, // fixed height or responsive( integer )
        preload: true, // load on document load or on button click
        borderRadius: 10, // set custom border radius for content
        onClosed: function () {
        console.log('onClosed'); // custom function
        },
        beforeClosed: function () {
        console.log("beforeClosed"); // custom function
        },
        onOpened: function () {
            jQuery('#zoom-buttons').css('display', 'none');

            console.log(jQuery('#zoom-buttons'));

        console.log('onOpened'); // custom function
        },
        beforeOpened: function () {
        console.log('beforeOpened'); // custom function
        }
    });



    // ALERTS OPTIONS
    toastr.options = {
        "positionClass": "toast-bottom-right"
    }

});

    function doNothing() {

    }


    function changeByAlgo() {

        var algoSelected = jQuery('#algo').val();

        var request = jQuery.ajax({
            method: "POST",
            url: URL + 'home/form',
            data: { 
                "algorithm": algoSelected 
            },
            dataType: "text"
        });

        request.done(function (response) {
            if(response != "ERROR") {
                jQuery("#mining").html(response);
                jQuery("#my_style").select2();
                jQuery("#my_premine").select2();
                jQuery("#my_pool").select2();
                jQuery("#my_explorer").select2();
                jQuery("#my_peers").select2();
                jQuery("#my_paper").select2();

                jQuery("#quote").html('0.50');

                // SHOW FIELDS
                jQuery('#coinpaymentButton').show();
            } else {
                jQuery('#mining').html('');
                jQuery("#quote").html('0.00');
                // HIDE FIELDS
                jQuery('#coinpaymentButton').hide();
            }
        });

        request.fail(function (a, b, c) { 
            console.log(a);
            console.log(b);
            console.log(c);
        });
    }


    function updateQuote(component) {

        var miningStyle = jQuery('#my_style').val();
        var premine = jQuery('#my_premine').val();
        var pool = jQuery('#my_pool').val();
        var explorer = jQuery('#my_explorer').val();
        var peers = jQuery('#my_peers').val();
        var paper = jQuery('#my_paper').val();


        var request = jQuery.ajax({
            method: "POST",
            url: URL + 'home/updateQuote',
            data: { 
                "style": miningStyle,
                "premine": premine,
                "pool": pool,
                "explorer": explorer,
                "peers": peers,
                "paper": paper
            },
            dataType: "text"
        });


        request.done(function (response) {
            jQuery('#quote').html(response);
        });

        request.fail(function (a, b, c) {
            jQuery('#quote').html('#error');
        });

    }


    function setupPayment() {

        var miningStyle = jQuery('#my_style').val();
        var premine = jQuery('#my_premine').val();
        var pool = jQuery('#my_pool').val();
        var explorer = jQuery('#my_explorer').val();
        var peers = jQuery('#my_peers').val();
        var paper = jQuery('#my_paper').val();
        var email = jQuery('#my_email').val();
        var check = jQuery('#checkOK');


        if(miningStyle === undefined || miningStyle === null || miningStyle.trim() === "") {
            toastr.warning("Mining Style not defined");
            jQuery('#my_style').focus();
            return;
        }

        if(premine === undefined || premine === null || premine.trim() === "") {
            toastr.warning("Premine not defined");
            jQuery('#my_premine').focus();
            return;
        }

        if(pool === undefined || pool === null || pool.trim() === "") {
            toastr.warning("Pool not defined");
            jQuery('#my_pool').focus();
            return;
        }

        if(explorer === undefined || explorer === null || explorer.trim() === "") {
            toastr.warning("Explorer not defined");
            jQuery('#my_explorer').focus();
            return;
        }

        if(peers === undefined || peers === null || peers.trim() === "") {
            toastr.warning("Peers not defined");
            jQuery('#my_peers').focus();
            return;
        }

        if(paper === undefined || paper === null || paper.trim() === "") {
            toastr.warning("Paper Wallet not defined");
            jQuery('#my_paper').focus();
            return;
        }


        if(email === undefined || email === null || email.trim() === "") {
            toastr.warning("Please fill the email form");
            jQuery('#my_email').focus();
            return;
        }

        var isChecked = jQuery(check).attr('checked');

        if(isChecked === undefined) {
            toastr.warning("Terms & Conditions not accepted");
            jQuery('#agree').focus();
            return;
        }


        // ONCE IT'S SELECTED
        var request = jQuery.ajax({
            method: "POST",
            url: URL + 'home/pay',
            data: { 
                "style": miningStyle,
                "premine": premine,
                "pool": pool,
                "explorer": explorer,
                "peers": peers,
                "paper": paper,
                "email": email,
                "check": isChecked
            },
            dataType: "text"
        });


        request.done(function (response) {

            if(response === "ERROR") {

            } else {

                var a = document.createElement('a');
                a.href = response;
                a.target = "_blank";
                a.click();

            }
        });

        request.fail(function (a, b, c) {
            toastr.error("Payment not available");
        });


    }

</script>

<div hidden>
    <a id="forPay">
</div>

<script type="text/javascript" src="<?=base_url();?>assets/js/daemons.js"></script>


</body>
</html>


