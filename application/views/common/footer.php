
<footer class="section footer-section section-pad-sm section-bg">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
            </div>
        </div>
    </div>
</footer>

<a href="#top" id="back-to-top" class=""></a>

<!-- Preloader !remove please if you do not want -->
<div id="preloader" style="display: none;">
    <div id="loader" style="display: none;"></div>
    <div class="loader-section loader-top"></div>
    <div class="loader-section loader-bottom"></div>
</div>
<!-- Preloader End -->

</div>

<script type="text/javascript">
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"http:\/\/cyc.saargo.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
    /* ]]> */
</script>
<script type="text/javascript" src="<?=base_url();?>assets/js/scripts.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/comment-reply.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/particles.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/script-pack.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/wp-embed.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/webfontloader.js"></script>
<script type="text/javascript">
    WebFont.load({google:{families:['Poppins:400,600,500:cyrillic,cyrillic-ext,devanagari,greek,greek-ext,khmer,latin,latin-ext,vietnamese,hebrew,arabic,bengali,gujarati,tamil,telugu,thai']}});
</script>
<script type="text/javascript" src="<?=base_url();?>assets/js/js_composer_front.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/vibox.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/toastr.min.js"></script>
