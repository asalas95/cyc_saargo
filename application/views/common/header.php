<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://cyc.saargo.com/xmlrpc.php">

    <style id="kirki-css-vars">:root{}</style>
    <title>CYC | Business Group</title>
    <link rel="shortcut icon" src="<?=base_url();?>assets/img/favicon.ico">
    <link rel="dns-prefetch" href="http://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="http://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Create Your Crypto » Feed" href="http://cyc.saargo.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Create Your Crypto » RSS comments" href="http://cyc.saargo.com/comments/feed/">
    <script type="text/javascript">
       window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/cyc.saargo.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
       !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
   </script>
   <script src="js/wp-emoji-release.min.js" type="text/javascript" defer=""></script>
   <style type="text/css">
   img.wp-smiley,
   img.emoji {
     display: inline !important;
     border: none !important;
     box-shadow: none !important;
     height: 1em !important;
     width: 1em !important;
     margin: 0 .07em !important;
     vertical-align: -0.1em !important;
     background: none !important;
     padding: 0 !important;
 }
</style>
<link rel="stylesheet" id="contact-form-7-css" href="<?=base_url();?>assets/css/styles.css" type="text/css" media="all">
<link rel="stylesheet" id="icos-fonts-css" href="<?=base_url();?>assets/css/css.css" type="text/css" media="all">
<link rel="stylesheet" id="vendor-css" href="<?=base_url();?>assets/css/vendor.bundle.css" type="text/css" media="all">
<link rel="stylesheet" id="icos-style-css" href="<?=base_url();?>assets/css/style.css" type="text/css" media="all">
<link rel="stylesheet" id="icos-lavender-css" href="<?=base_url();?>assets/css/lavender.css" type="text/css" media="all">
<link rel="stylesheet" id="icos-muscari-css" href="<?=base_url();?>assets/css/muscari.css" type="text/css" media="all">
<link rel="stylesheet" id="icos-lobelia-css" href="<?=base_url();?>assets/css/lobelia.css" type="text/css" media="all">
<link rel="stylesheet" id="icos-theme-css" href="<?=base_url();?>assets/css/theme-java.css" type="text/css" media="all">
<link rel="stylesheet" id="js_composer_front-css" href="<?=base_url();?>assets/css/js_composer.min.css" type="text/css" media="all">
<link rel="stylesheet" id="kirki-styles-icos-css" href="<?=base_url();?>assets/css/kirki-styles.css" type="text/css" media="all">
<style id="kirki-styles-icos-inline-css" type="text/css">
.site-header .navbar-brand img{height:40px;}
.site-header .navbar-brand{margin-top:20px;margin-bottom:20px;margin-left:0;margin-right:15px;}
.has-fixed.site-header .navbar-brand img{height:40px;}
.has-fixed.site-header .navbar-brand{margin-top:20px;margin-bottom:20px;margin-left:0;margin-right:15px;}
.page-banner{min-height:280px;}
h2.page-heading{font-size:34px;}
footer.footer-section{padding-top:0px;}
</style>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery-migrate.min.js"></script>
<link rel="https://api.w.org/" href="http://cyc.saargo.com/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://cyc.saargo.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://cyc.saargo.com/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.9.8">
<link rel="canonical" href="http://cyc.saargo.com/">
<link rel="shortlink" href="http://cyc.saargo.com/">
<link rel="alternate" type="application/json+oembed" href="http://cyc.saargo.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fcyc.saargo.com%2F">
<link rel="alternate" type="text/xml+oembed" href="http://cyc.saargo.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fcyc.saargo.com%2F&amp;format=xml">
<style type="text/css">
@media only screen and (max-width: 991px) { 
    .site-header .navbar.is-transparent { position: absolute; }
    .admin-bar .site-header .navbar.is-transparent { top: 0; } }
</style>
<style type="text/css">
.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}
</style>

<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress.">
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://cyc.saargo.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->

<style type="text/css" data-type="vc_custom-css">
.animated{
  animation-duration:1s;
  animation-fill-mode:both
}
.animated:not(.vc_grid-item) {
  animation-duration: .6s;
  visibility: hidden;
}
</style>

<style type="text/css" data-type="vc_shortcodes-custom-css">
.vc_custom_1538371421426{margin-bottom: 35px !important;}
.vc_custom_1529897707202{margin-bottom: 18px !important;}
.vc_custom_1538371828084{margin-top: 50px !important;margin-bottom: 60px !important;}
.vc_custom_1529898206614{margin-top: 50px !important;margin-bottom: 60px !important;}
.vc_custom_1529898312582{margin-top: 50px !important;margin-bottom: 60px !important;}
.vc_custom_1529488984202{margin-right: 0px !important;margin-left: 0px !important;}
.vc_custom_1538372797375{margin-top: 50px !important;margin-bottom: 60px !important;}
.vc_custom_1529898500400{margin-top: 50px !important;margin-bottom: 60px !important;}
.vc_custom_1520830144361{padding-bottom: 60px !important;}
.vc_custom_1529898628026{margin-top: 50px !important;}
.vc_custom_1520830144361{padding-bottom: 60px !important;}
.vc_custom_1529898992425{margin-top: 50px !important;padding-bottom: 60px !important;}
.vc_custom_1520917253885{padding-bottom: 60px !important;}
.vc_custom_1529899234573{margin-top: 50px !important;padding-bottom: 60px !important;}
.vc_custom_1529899275903{margin-top: 50px !important;padding-bottom: 60px !important;}
.vc_custom_1520996880206{padding-bottom: 60px !important;}
</style>
<noscript>
    <style type="text/css"> 
    .wpb_animate_when_almost_visible { opacity: 1; }
</style>
</noscript>

<link rel="stylesheet" href="<?=base_url();?>assets/css/css(1).css" media="all">    
<link rel="stylesheet" href="<?=base_url();?>assets/css/vibox.css" media="all">  
<link rel="stylesheet" href="<?=base_url();?>assets/css/toastr.min.css" media="all">  