<!doctype html>
<html lang="en">
<head>
  <title>CYC - Contact</title>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="<?=base_url();?>assets/css/mainSaargo.css">
</head>
<body class="page page-onboarding preload">

  <main>

    <a href="index.html" class="button button-close" role="button"></a>

    <section class="row no-gutter reverse-order">
      <div class="col-one-half middle padding">
        <div class="max-width-s">
          <h5>Write us!</h5>
          <p class="paragraph">We are attentive to any question.</p>
          
          <form class="contact__form" method="POST" action="Contact/mail">
	        <!-- form message -->
            <div class="row">
                <div class="alert alert-success contact__msg" style="display: none" role="alert">
                    Your message has been sent correctly
                </div>
            </div>
            <!-- end message -->
            <div class="form-group">
              <label for="name">Name:</label>
              <input id="name" type="text" name="name" required>
            </div>
            <div class="form-group">
              <label for="email">Email:</label>
              <input id="email" type="email" name="email" required>
            </div>
            <div class="form-group">
              <label for="message">Message:</label>
              <textarea id="message" name="message" required></textarea>
            </div>
            <input name="submit" type="submit" class="button button-primary full-width space-top submit" role="button" value="Send question">
          </form>
          
        </div>
      </div>
      <div class="col-one-half bg-image-02 featured-image"></div>
    </section>

  </main>

  <script src="js/jquery.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/mail.js"></script>


</body>
</html>