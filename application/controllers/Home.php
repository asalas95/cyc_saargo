<?php
defined('BASEPATH') OR exit('No direct script access allowed');




class Home extends CI_Controller {

    


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();


		// DECLARE KEYS
		$this->encrypt_method = "AES-256-CBC";
    	$this->secret_key = 'dummyluka';
    	$this->secret_iv = 'lukadummy';

    	// HASH
    	$this->key = hash('sha256', $this->secret_key);
    	$this->iv = substr(hash('sha256', $this->secret_iv), 0, 16);

        // OPTIONS
        $this->YES = "bCz2MKZlXNym71qRrvpEdg==";
        $this->NO = "wbQ/nqxazpq/f7nPbHl4iw==";

		// DECLARE ALGOS
		$this->CN = "/fRIOShLQoYUjB/cKv0k9YBZsVm4ATUukBEnJP25pqs=";
		$this->SCRYPT = "ZD84VtbfKKSEDhyWDQlCNw==";
		$this->ETHASH = "IT5DfXN4e4d80btOLZ+lzA==";
		$this->X11 = "0BqVcnAqyWCI5KbKo53QHg==";
		$this->SHA256 = "nLZwjCtxf4tjr3fZU/F+iQ==";
		$this->NIST5 = "mLx80FPYEl9LmDW2YQMOgQ==";
		$this->NEOSCRYPT = "LcYP1whYNjVQMVPUG7STTg==";


		// DECLARE VALUES
        $this->ETHASH_VALUE = 0.50;

        $this->CN_VALUE = 0.50;
        $this->CN_MINING_POW_VALUE = 0.00;
        $this->CN_MINING_MASTERNODE_VALUE = 0.50;
        $this->CN_POOL_VALUE = 0.20;
        $this->CN_EXPLORER_VALUE = 0.15;
        $this->CN_PEERS_VALUE = 0.15;
        $this->CN_PAPER_VALUE = 0.15;


		$this->SCRYPT_VALUE = 0.50;


		$this->load->helper('url');
        $this->load->helper('form');



	}  


	public function index()
	{
		$this->load->view('home');
	}

    public function agreements()
    {
        $this->load->view('agreements');
    }


    public function initForm() {
    	
    	$form = "	<div>";
    	$form .= "		<label>Algorithm</label>";
    	$form .= "		<select id='algo' onchange='changeByAlgo()'>";
    	$form .= "			<option value=''>Select an Algorithm</option>";
        $form .= "          <option value='".$this->CN."'>CryptoNote (Masternodes Optionally) | &nbsp&nbsp&nbspɃ ".$this->CN_VALUE."</option>";
    #   $form .= "          <option value='".$this->ETHASH."'>Ethash (POW) | &nbsp&nbsp&nbsp Ƀ 0,50</option>";
    #	$form .= "			<option value='".$this->SCRYPT."'>Scrypt (POW) | Ƀ 0,50</option>";
    #	$form .= "			<option value='".$this->X11."'>X11 Ƀ 0,03</option>";
    #	$form .= "			<option value='".$this->SHA256."'>SHA256 Ƀ 0,03</option>";
    #	$form .= "			<option value='".$this->NIST5."'>Nist Ƀ 0,03</option>";
    #	$form .= "			<option value='".$this->NEOSCRYPT."'>NeoScrypt Ƀ 0,03</option>";
    	$form .= "		</select>";
    	$form .= "		<div id='mining'></div>";
    	$form .= "	</div>";

    	echo $form;
    }


    public function updateQuote() {

        $totalQuote = $this->CN_VALUE; // 0.50 BASE


        $style = openssl_decrypt(base64_decode($_POST['style']), $this->encrypt_method, $this->key, 1, $this->iv);
        $premine = openssl_decrypt(base64_decode($_POST['premine']), $this->encrypt_method, $this->key, 1, $this->iv);
        $pool = openssl_decrypt(base64_decode($_POST['pool']), $this->encrypt_method, $this->key, 1, $this->iv); 
        $explorer = openssl_decrypt(base64_decode($_POST['explorer']), $this->encrypt_method, $this->key, 1, $this->iv);
        $peers = openssl_decrypt(base64_decode($_POST['peers']), $this->encrypt_method, $this->key, 1, $this->iv); 
        $paper = openssl_decrypt(base64_decode($_POST['paper']), $this->encrypt_method, $this->key, 1, $this->iv);


        if($style === "YES") {
            $totalQuote += $this->CN_MINING_MASTERNODE_VALUE;
        } else {
            $totalQuote += $this->CN_MINING_POW_VALUE;
        }

        if($premine === "YES") {
            $totalQuote += 0.00;
        }

        if($pool === "YES") {
            $totalQuote += $this->CN_POOL_VALUE;
        } else {
            $totalQuote += 0.00;
        }

        if($explorer === "YES") {
            $totalQuote += $this->CN_EXPLORER_VALUE;
        } else {
            $totalQuote += 0.00;
        }

        if($peers === "YES") {
            $totalQuote += $this->CN_PEERS_VALUE;
        } else {
            $totalQuote += 0.00;
        }

        if($paper === "YES") {
            $totalQuote += $this->CN_PAPER_VALUE;
        } else {
            $totalQuote += 0.00;
        }

        echo $totalQuote;
    }



    public function form() {

    	if(isset($_POST['algorithm'])) {

    		// GET VALUES
    		$algo = openssl_decrypt(base64_decode($_POST['algorithm']), $this->encrypt_method, $this->key, 1, $this->iv);


    		switch ($algo) {
    			case 'ALGO-CRYPTONIGHT':
    				
                    $bytecoin = "<a href='https://bytecoin.org/' target='_blank'>Bytecoin</a>";
                    $monero = "<a href='https://www.getmonero.org/' target='_blank'>Monero</a>";

    				$output = "";
    				$output .= "<br/>";
    				$output .= "<div>";
    				$output .= "<p>CryptoNote is an application layer protocol that powers several decentralized digital currencies oriented towards privacy such as ".$monero.", ".$bytecoin.", etc.</p>";
    				$output .= "<div>";

    				
                    # MINING STYLE 
    				$output .= "<div>";
    				$output .= "	<label>Mining Style:</label>";
    				$output .= "	<select id='my_style' onchange='updateQuote(this)'>";
    				$output .= "		<option value=''>Select a mining style</option>";
    				$output .= "		<option value='".$this->NO."'>POW (CN CLASSIC) | Ƀ FREE</option>";
    				$output .= "		<option value='".$this->YES."'>POW & Masternodes (CN HEAVY) | Ƀ ".$this->CN_MINING_MASTERNODE_VALUE."</option>";
    				$output .= "	</select>";
    				$output .= "</div>";


    				# PREMINE 
    				$output .= "<div>";
    				$output .= "	<label style='margin-top: 2vh'>Premine?: </label>";
    				$output .= "	<select id='my_premine' onchange='updateQuote(this)'>";
    				$output .= "		<option value=''>Select premine mode</option>";
    				$output .= "		<option value='".$this->NO."'>NO | Ƀ FREE</option>";
    				$output .= "		<option value='".$this->YES."'>YES | Ƀ FREE</option>";
    				$output .= "	</select>";
    				$output .= "</div>";


    				# POOL
    				$output .= "<div>";
    				$output .= "	<label style='margin-top: 2vh'>Mining Pool ⤳ ";
                    $output .= "        <a href='http://pool.sevabit.com' target='_blank'>Example</a>";
                    $output .= "    </label>";
    				$output .= "	<select id='my_pool' onchange='updateQuote(this)'>";
    				$output .= "		<option value=''>Select your pool</option>";
    				$output .= "		<option value='".$this->NO."'>NO | Ƀ FREE</option>";
    				$output .= "		<option value='".$this->YES."'>YES | Ƀ ".$this->CN_POOL_VALUE."</option>";
    				$output .= "	</select>";
    				$output .= "</div>";


    				# EXPLORER
    				$output .= "<div>";
    				$output .= "	<label style='margin-top: 2vh'>Block Explorer ⤳ ";
                    $output .= "        <a href='http://explorer.sevabit.com' target='_blank'>Example</a>";
                    $output .= "    </label>";
    				$output .= "	<select id='my_explorer' onchange='updateQuote(this)'>";
    				$output .= "		<option value=''>Select your explorer</option>";
    				$output .= "		<option value='".$this->NO."'>NO | Ƀ FREE</option>";
    				$output .= "		<option value='".$this->YES."'>YES | Ƀ ".$this->CN_EXPLORER_VALUE."</option>";
    				$output .= "	</select>";
    				$output .= "</div>";


    				# PEERS MAP
    				$output .= "<div>";
    				$output .= "	<label style='margin-top: 2vh'>Peers Map ⤳ ";
                    $output .= "        <a href='http://map.sevabit.com' target='_blank'>Example</a>";
                    $output .= "    </label>";
    				$output .= "	<select id='my_peers' onchange='updateQuote(this)'>";
    				$output .= "		<option value=''>Select your peers map</option>";
    				$output .= "		<option value='".$this->NO."'>NO | Ƀ FREE</option>";
    				$output .= "		<option value='".$this->YES."'>YES | Ƀ ".$this->CN_PEERS_VALUE."</option>";
    				$output .= "	</select>";
    				$output .= "</div>";


                    # PAPER WALLET 
                    $output .= "<div>";
                    $output .= "    <label style='margin-top: 2vh'>Paper Wallet Online ⤳ ";
                    $output .= "        <a href='https://paperwallet.b2bcoin.xyz' target='_blank'>Example</a>";
                    $output .= "    </label>";
                    $output .= "    <select id='my_paper' onchange='updateQuote(this)'>";
                    $output .= "        <option value=''>Select your paper wallet</option>";
                    $output .= "        <option value='".$this->NO."'>NO | Ƀ FREE</option>";
                    $output .= "        <option value='".$this->YES."'>YES | Ƀ ".$this->CN_PAPER_VALUE."</option>";
                    $output .= "    </select>";
                    $output .= "</div>";


                    # EMAIL@ MAP
                    $output .= "<div style='margin-top: 2vh;'>";
                    $output .= "<span style='width: 40%; display: inline'>";
                    $output .= "    <label >Your receiver email:</label>";
                    $output .= "<span />";
                    $output .= "<span style='width: 50%; display: inline; float: right'>";
                //  $output .= "    <input type='email' id='my_email' style='width: 100%' />";
                    $output .= "<label clas='name-label' style='width: 100%'>";
                    $output .= "    <svg class='icon icon-envelop'>";
                    $output .= "        <use xlink:href='#icon-envelop'></use>";
                    $output .= "    </svg>";
                    $output .= "    <input id='my_email' type='email' placeholder='Email' style='width: 100%'/>";
                    $output .= "</label>";

                    $output .= "<span />";
                    $output .= "</div>";


    				break;
				case 'ALGO-SCRYPT':
    				# code...
    				$output = "WORKS1";
    				break;
				case 'ALGO-ETHASH':
    				# code...
    				$output = "WORKS1";
    				break;
				case 'ALGO-X11':
    				# code...
    				$output = "WORKS1";
    				break;
				case 'ALGO-SHA256':
    				# code...
    				$output = "WORKS1";
    				break;
				case 'ALGO-NIST5':
    				# code...
    				$output = "WORKS1";
    				break;
				case 'ALGO-NEOSCRYPT':
    				# code...
    				$output = "WORKS1";
    				break;
    			
    			default:
    				$output = "ERROR";
    				break;
    		}

    	} else {
    		$output = "ERROR";
    	}

    	echo $output;

    }



    public function pay() {

        require_once(FCPATH.'assets/payments/CoinpaymentsAPI.php');
        require_once(FCPATH.'assets/payments/keys.php');


        // PROCESS THE DATA
        $totalQuote = $this->CN_VALUE; // 0.50 BASE


        $style = openssl_decrypt(base64_decode($_POST['style']), $this->encrypt_method, $this->key, 1, $this->iv);
        $premine = openssl_decrypt(base64_decode($_POST['premine']), $this->encrypt_method, $this->key, 1, $this->iv);
        $pool = openssl_decrypt(base64_decode($_POST['pool']), $this->encrypt_method, $this->key, 1, $this->iv); 
        $explorer = openssl_decrypt(base64_decode($_POST['explorer']), $this->encrypt_method, $this->key, 1, $this->iv);
        $peers = openssl_decrypt(base64_decode($_POST['peers']), $this->encrypt_method, $this->key, 1, $this->iv); 
        $paper = openssl_decrypt(base64_decode($_POST['paper']), $this->encrypt_method, $this->key, 1, $this->iv);

        $email = $_POST['email'];



        if($style === "YES") {
            $totalQuote += $this->CN_MINING_MASTERNODE_VALUE;
        } else if ($style === "NO") {
            $totalQuote += $this->CN_MINING_POW_VALUE;
        } else {
            echo "ERROR";
            return;
        }

        if($premine === "YES") {
            $totalQuote += 0.00;
        } else if ($premine === "NO") {
            $totalQuote += 0.00;
        } else {
            echo "ERROR";
            return;
        }

        if($pool === "YES") {
            $totalQuote += $this->CN_POOL_VALUE;
        } else if ($pool === "NO") {
            $totalQuote += 0.00;
        } else {
            echo "ERROR";
            return;
        }

        if($explorer === "YES") {
            $totalQuote += $this->CN_EXPLORER_VALUE;
        } else if ($explorer === "NO") {
            $totalQuote += 0.00;
        } else {
            echo "ERROR";
            return;
        }

        if($peers === "YES") {
            $totalQuote += $this->CN_PEERS_VALUE;
        } else if ($peers === "NO") {
            $totalQuote += 0.00;
        } else {
            echo "ERROR";
            return;
        }

        if($paper === "YES") {
            $totalQuote += $this->CN_PAPER_VALUE;
        } else if ($paper === "NO") {
            $totalQuote += 0.00;
        } else {
            echo "ERROR";
            return;
        }




        try {

            // REQUEST THE API
            $cps_api = new CoinpaymentsAPI($private_key, $public_key, 'json');
            
            // SENDS THE TX
            $txSender = $cps_api->CreateSimpleTransaction($totalQuote, 'BTC', $email, 'C MAMO');

            // RETURNS THE URL OF TRANSACTION
            echo $txSender['result']['status_url'];
            return;
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            exit();
        }
        
    }
}

