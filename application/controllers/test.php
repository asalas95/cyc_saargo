<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();

		// DECLARE KEYS
		$this->encrypt_method = "AES-256-CBC";
    	$this->secret_key = 'dummyluka';
    	$this->secret_iv = 'lukadummy';

    	// HASH
    	$this->key = hash('sha256', $this->secret_key);
    	$this->iv = substr(hash('sha256', $this->secret_iv), 0, 16);

        // OPTIONS
        $this->YES = "bCz2MKZlXNym71qRrvpEdg==";
        $this->NO = "wbQ/nqxazpq/f7nPbHl4iw==";

		$this->load->helper('url');
        $this->load->helper('form');

	}  


	public function index()
	{
		$this->load->view('home');
	}



    public function decrypt() {

       echo openssl_decrypt(base64_decode($this->NO), $this->encrypt_method, $this->key, 1, $this->iv);

    }

}

